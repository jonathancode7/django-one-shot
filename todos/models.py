from django.db import models

# Create your models here.


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    # this makes the name show up in admin page instead of a generic name
    # it would look like this -TodoList object (1)


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    todolist = models.ForeignKey(
        TodoList,
        related_names="items",
        on_delete=models.CASCADE,
    )


class Meta:
    ordering = ["item_number"]
